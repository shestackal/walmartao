# -*- coding: utf-8 -*-

# import datetime #unused
from datetime import datetime
import json
import time
import logging
import os.path
import pickle
from random import randint
import re

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
import urllib

from urlparse import urlparse



def print_error(text):
    logging.error(text)
    print(text)


class ApiClient(object):
    def __init__(self, endpoint):
        self.endpoint = endpoint

    def get_json(self, tries_count=4):
        for i in range(tries_count):
            try:
                response = urllib.urlopen(self.endpoint)
                data = json.loads(response.read())
            except:
                pass
            else:
                return data

        print 'Error loading json'
        raise


class Walmart(object):
    def __init__(self, order_id, param,
                 local_storage='/var/www/api/public/tmp/',
                 cookies_storage='/cookies/',
                 proxy=None):
        self.order_id = order_id
        self.param = param
        self.basketAttempt = 0
        self.costPrice = 0

        self.local_storage = local_storage
        self.cookies_storage = cookies_storage
        self.proxy_cfg = proxy

    def load_order_details(self, data):
        if 'error' in data:
            print 'ERROR: ' + self.data['error']
            raise

        print 'got order data'

        self.data = data

        self.user = self.data['user']
        self.delete_address = self.data['delete_address']

        if self.param == 'address':
            self.data['place_order'] = 0

        if self.param == 'place':
            self.data['place_order'] = 1

        self.address = self.data['address']
        self.url = self.data['url']
        o = urlparse(self.url)
        self.domain = o.netloc
        self.items = self.data['items']
        self.checkCost = True
        for (i, item) in enumerate(self.data['items']):
            print 'SKU:' + item
            print 'URL:' + self.data['items'][item]['url']

    def startDriver(self, phantomjs=True, attempts=2, remote=None):
        kwargs_chrome = {}
        if self.proxy_cfg:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--proxy-server=%s' % self.proxy_cfg)
            kwargs_chrome['chrome_options'] = chrome_options

        for i in range(attempts):
            try:
                if phantomjs:
                    webdriver.DesiredCapabilities.PHANTOMJS["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit")
                    self.driver = webdriver.PhantomJS()
                elif remote:
                    #self.driver = webdriver.Remote(remote, DesiredCapabilities.CHROME, **kwargs_chrome)
                    self.driver = webdriver.Remote(remote, DesiredCapabilities.CHROME)
                else:
                    self.driver = webdriver.Chrome(**kwargs_chrome)

            except:
                time.sleep(2)

            else:
                # TODO: Move to settings
                self.driver.set_page_load_timeout(120)
                self.driver.set_script_timeout(120)
                return

        raise WebdriverError

    def get_cookie_file(self):
        cookieFile = self.cookies_storage + "cookie_walmart_" + self.user['email'].replace("@", "_").replace(".", "_") + ".pkl"
        if os.path.isfile(cookieFile):
            return cookieFile
        return False

    def loadCookies(self):
        if self.get_cookie_file() != False:
            cookies = pickle.load(open(self.get_cookie_file(), "rb"))
            for cookie in cookies:
                try:
                    self.driver.add_cookie(cookie)
                except:
                    nothing = False

    def deleteCookie(self):
        cookieFile = self.cookies_storage + "cookie_walmart_" + self.user['email'].replace("@", "_").replace(".", "_") + ".pkl"
        if os.path.isfile(cookieFile):
            os.remove(cookieFile)

    def saveCookies(self):
        cookieFile = self.cookies_storage + "cookie_walmart_" + self.user['email'].replace("@", "_").replace(".", "_") + ".pkl"
        self.deleteCookie()
        cookies = self.driver.get_cookies()
        cookies_file = open(cookieFile, 'wb')
        pickle.dump(cookies, cookies_file)

    def waitUntilLoaded(self, force=None):
        time.sleep(randint(1, 2))
        ts = int(time.time())
        loaded = False
        while int(time.time()) - ts < 15:
            try:
                alert = self.driver.switch_to.alert
                alert.accept()
            except:
                pass

            try:
                loaded = self.driver.execute_script("return document.readyState;") == "complete"
            except:
                loaded = False
            if loaded:
                break

        if not loaded:
            try:
                self.driver.execute_script("return window.stop;")
            except:
                loaded = False
        try:
            alert = self.driver.switch_to.alert
            alert.accept()
        except:
            pass

        if force is not None:
            return

        if not loaded:
            try:
                print 'Refresh page...'
                self.driver.refresh()
            except:
                loaded = False
            return self.waitUntilLoaded(True)
        time.sleep(randint(1, 5))
        return

    def elementExists(self, selector, item=False):
        try:
            if item == False:
                check = self.driver.find_elements_by_css_selector(selector)
            else:
                check = item.find_elements_by_css_selector(selector)
            if len(check) > 0:
                return True
        except:
            return False

        return False

    def loadUrl(self, page, tryAgain = 3):
        try:
            self.driver.get(page)
        except:
            if tryAgain > 0:
                time.sleep(1)
                tryAgain = tryAgain - 1
                return self.loadUrl(page, tryAgain)
            print 'Impossible load the page: ' + page
            self.completeProcess()

    def screenshots(self):
        orders = self.order_id.split('^^')
        try:
            html_source = self.driver.page_source
        except:
            html_source = ''

        screenshot = False
        str_datetime = datetime.now().strftime('%Y%m%d_%H%M%S')
        for (i, item) in enumerate(orders):
            try:
                self.driver.get_screenshot_as_file(self.local_storage + '/errors/' + str_datetime + '_' + item + '.png')
                screenshot = True
            except:
                pass
            try:
                text_file = open(self.local_storage + '/errors/' + str_datetime + '_' + item + '.html', "w")
                text_file.write(html_source.encode('utf-8').strip())
                text_file.close()
            except:
                pass
        if screenshot:
            print "SCREENSHOT"

    def completeProcess(self, saveCookies=True, dont_close_broser=False):
        print('completeProcess')
        if saveCookies == True:
            try:
                self.saveCookies()
                cookie = True
            except:
                cookie = False
        else:
            self.deleteCookie()
        self.screenshots()

        if dont_close_broser:
            input()
        self.driver.quit()

        raise RuntimeError('completeProcess exit')

    def write(self, selector, text, fast=False):
        value = ''
        try:
            self.driver.find_element_by_css_selector(selector).clear()
        except:
            value = ''
        if value == '':
            for c in text:
                if fast:
                    t = float((randint(1,2)))/100
                else:
                    t = float((randint(2,10)))/100
                self.driver.find_element_by_css_selector(selector).send_keys(c)
                time.sleep(t)

    def writeObj(self, obj, text, fast=False):
        value = ''
        try:
            value = obj.get_attribute('value')
        except:
            value = ''
        if value == '':
            for c in text:
                if fast:
                    t = float((randint(2,4))) / 10
                else:
                    t = float((randint(2,10))) / 10
                obj.send_keys(c)
                time.sleep(t)

    def safeClick(self, selector, attempt=6):
        if attempt < 0:
            return False
        try:
            self.driver.find_element_by_css_selector(selector).click()
            time.sleep(randint(1, 3))
            return True
        except:
            attempt = attempt - 1
            time.sleep(2)
            return self.safeClick(selector, attempt)

    def safeClickObj(self, obj, attempt=6):
        if attempt < 0:
            return False
        try:
            obj.click()
            return True
        except:
            attempt = attempt - 1
            time.sleep(2)
            return self.safeClickObj(obj, attempt)

    def is_checked(self, item):
        checked = self.driver.find_element_by_css_selector(item).is_selected()
        return checked

    def doLogin(self, attempt=4):
        print 'Login'
        try:
            self.write('.form-field-email input', self.user['email'], True)
            self.write('.form-field-password input', self.user['password'], True)
        except:
            print "Impossible to login. Please try again"
            self.completeProcess()

        self.activateCheckbox('.remember-me')
        if not self.safeClick('.form-actions [type="submit"]'):
            print 'no submit button in login page'
            self.completeProcess(False)

        self.waitUntilLoaded()
        time.sleep(1)

        self.solveReCaptcha(attempt)

        if self.elementExists('.captcha.re-captcha'):
            print 'Impossible login'
            self.completeProcess()

        #check if the login detail are correct
        if self.elementExists('.alert.active.alert-error'):
            print 'Wrong login details'
            self.completeProcess(False)

    def solveReCaptcha(self, attempt):
        if self.elementExists('.captcha.re-captcha'):
            print 'Re-Captcha'
            time.sleep(10)
            return

    def isLoginPage(self):
        return self.elementExists('.form-field-email input') and self.elementExists('.form-field-password input')

    def setInputValue(self, cssSelector, value):
        try:
            inputField = self.driver.find_element_by_css_selector(cssSelector)
            self.driver.execute_script("arguments[0].setAttribute('value', '" + value + "')", inputField)
        except NoSuchElementException:
            print "Impossible find element " + cssSelector
            self.completeProcess()
        except:
            print "Error setting the value " + value + " in " + cssSelector
            self.completeProcess()

    def setSelectValue(self, cssSelector, value, exception = True):
        if exception:
            try:
                select = Select(self.driver.find_element_by_css_selector(cssSelector))
                select.select_by_value(value)
            except NoSuchElementException:
                print "Impossible find element " + cssSelector
                self.completeProcess()
            except:
                print "Error setting the value " + value + " in " + cssSelector
                self.completeProcess()
        else:
            select = Select(self.driver.find_element_by_css_selector(cssSelector))
            select.select_by_value(value)

    def activateCheckbox(self, selector):
        try:
            elem = self.driver.find_element_by_css_selector(selector)
            if elem.get_attribute('checked') == None:
                elem.click()
        except:
            pass

    def addAddress(self, address=None, string=''):
        address = address or self.address

        self.loadUrl('https://www.walmart.com/account/shippingaddresses')
        self.waitUntilLoaded()

        if self.isLoginPage():
            self.doLogin()

        if self.elementExists('.add-new-address-btn'):
            self.safeClick('.add-new-address-btn')
            self.waitUntilLoaded()

        if self.elementExists('[data-automation-id="new-address-tile-add-new-address"]'):
            self.safeClick('[data-automation-id="new-address-tile-add-new-address"]')
            self.waitUntilLoaded()

        if not self.elementExists('#firstName'):
            print string + 'Impossible add the address'
            self.completeProcess()

        print string + 'Add the address'

        input('check the adress page')

        #try to separate name
        firstname = address[0].split(' ')[0]
        lastname = ' '.join(address[0].split(' ')[1:])
        if lastname == '':
            lastname = firstname

        self.write("#firstName", firstname.replace("'", "\\'"), True)
        self.write("#lastName", lastname.replace("'", "\\'"), True)
        self.write("#phone", address[6].replace("'", "\\'").replace(" ", ""), True)
        self.write("#addressLineOne", address[1], True)
        self.write("#addressLineTwo", address[2], True)
        self.write("#city", address[3].replace("'", "\\'"), True)
        self.write("#postalCode", address[5], True)

        if self.is_checked('#isDefault'):
            self.safeClick('.form-preferred-address-container span')

        self.setSelectValue('#state', address[4])

        confirmed = self.safeClick('.save-address')
        if not confirmed:
            print string + 'Impossible to confirm the address'
            self.completeProcess()
        self.waitUntilLoaded()

        # sometimes Walmart asks for the password
        if self.elementExists('form.option-form-control') and self.elementExists('form.option-form-control [type="password"]'):
            print string + 'Walmart password'
            self.write('form.option-form-control [type="password"]', self.user['password'], True)
            self.safeClick('form.option-form-control [type="submit"]')
            self.waitUntilLoaded()
            self.checkAddressSpinner()
            self.safeClick('[data-automation-id="address-form-submit"]')
            self.waitUntilLoaded()


        if self.elementExists('.alert.active.alert-error'):
            print string + 'Error in the address'
            self.completeProcess()

        print string + 'Address added'

    def addAddress2(self, address=False, string=''):
        if address == False:
            address = self.address

        if self.isLoginPage():
            self.doLogin()

        if self.elementExists('[data-automation-id="shipping-address"] .CXO_module_header_edit_button'):
            self.safeClick('[data-automation-id="shipping-address"] .CXO_module_header_edit_button')
            self.waitUntilLoaded()

        if self.isLoginPage():
            self.doLogin()

        if self.elementExists('.add-new-address-btn'):
            self.safeClick('.add-new-address-btn')
            self.waitUntilLoaded()

        if self.elementExists('[data-automation-id="new-address-tile-add-new-address"]'):
            self.safeClick('[data-automation-id="new-address-tile-add-new-address"]')
            self.waitUntilLoaded()

        if not self.elementExists('[name="firstName"]'):
            print 'Impossible to add the address'
            self.completeProcess()

        print string + 'Add the address'
        #input('check adress page')
        # try to separate name
        firstname = address[0].split(' ')[0]
        lastname = ' '.join(address[0].split(' ')[1:])
        if lastname == '':
            lastname = firstname

        self.write('[name="firstName"]', firstname.replace("'", "\\'"))
        time.sleep(randint(1, 2))
        self.write('[name="lastName"]', lastname.replace("'", "\\'"))
        time.sleep(randint(1, 2))
        self.write('[name="phone"]', address[6].replace("'", "\\'").replace(" ", ""))
        time.sleep(randint(1, 2))
        self.write('[name="addressLineOne"]', address[1])
        time.sleep(randint(1, 2))
        self.write('[name="addressLineTwo"]', address[2])
        time.sleep(randint(1, 2))
        self.write('[name="city"]', address[3].replace("'", "\\'"))
        time.sleep(randint(1, 2))
        self.write('[name="postalCode"]', address[5])
        time.sleep(randint(1, 2))
        self.setSelectValue('[name="state"]', address[4])
        time.sleep(randint(1, 2))

        confirmed = self.safeClick('[data-automation-id="address-form-submit"]') or self.safeClick('[data-automation-id="address-book-action-buttons-on-continue"]')
        if not confirmed:
            print string + 'Impossible to confirm the address'
            self.completeProcess()
        self.waitUntilLoaded()

        if self.elementExists('.button-save-address'):
            self.safeClick('.button-save-address')
            print string + 'Address added'
            self.waitUntilLoaded()

        if self.elementExists('.alert.active.alert-error'):
            print string + 'Error in the address'
            self.completeProcess()

        if self.elementExists('.alert.active.alert-warning'):
            self.safeClick('.button-save-address')
            print string + 'Address warning'
            self.waitUntilLoaded()

        #sometimes Walmart asks for the password
        if self.elementExists('form.option-checkout') and self.elementExists('form.option-checkout [type="password"]'):
            print string + 'Walmart password'
            self.write('form.option-checkout [type="password"]', self.user['password'], True)
            self.safeClick('form.option-checkout [type="submit"]')
            self.waitUntilLoaded()
            self.checkAddressSpinner()
            self.safeClick('[data-automation-id="address-form-submit"]')
            self.waitUntilLoaded()

        self.solveReCaptcha(0)

        self.safeClick('[data-automation-id="address-book-action-buttons-on-continue"]')
        if self.elementExists('[data-automation-id="address-validation-message-save-address"]'):
            self.safeClick('[data-automation-id="address-validation-message-save-address"]')
            print string + 'Confirm address'
            self.waitUntilLoaded()
            if self.elementExists('[data-automation-id="address-book-action-buttons-on-continue"]'):
                self.safeClick('[data-automation-id="address-book-action-buttons-on-continue"]')
                print string + 'Confirmed address'
                self.waitUntilLoaded()




    def getBasketItems(self):
        if self.elementExists('.header-Cart-count'):
            return self.driver.find_element_by_css_selector('.header-Cart-count').text
        return 0

    def emptyCart(self):
        objects = self.driver.find_elements_by_css_selector('[data-automation-id="cart-item-remove"]')
        for (i, item) in enumerate(objects):
            self.safeClickObj(item)
            self.waitUntilLoaded()
            time.sleep(3)

        objects = self.driver.find_elements_by_css_selector('[data-automation-id="cart-btn-remove"]')
        for (i, item) in enumerate(objects):
            self.safeClickObj(item)
            self.waitUntilLoaded()
            time.sleep(3)

        self.waitUntilLoaded()
        return

    def checkBasket(self):
        if self.basketAttempt > 10:
            print "Impossible empty the basket"
            self.completeProcess()
            return
        self.basketAttempt = self.basketAttempt + 1
        numItems = self.getBasketItems()
        if numItems != "" and int(numItems) > 0:
            print "Go to the cart page"
            self.goToCartPage()

            print "Empty the cart..."
            self.emptyCart()

            # go to the offers page
            self.loadUrl(self.url)
            self.waitUntilLoaded()
            self.checkBasket()

    def goToOffersPage(self, mainUrl):
        try:
            a = re.search('product\/(\d+)\/sellers', self.driver.current_url)
            sku = a.group(1)
        except:
            sku = ''

        if sku != '':
            print 'Going to the product page'
            tmpUrl = 'https://www.walmart.com/ip/' + sku
            self.loadUrl(tmpUrl)
            self.waitUntilLoaded()
            clicked = False
            if self.elementExists('.btn-compare'):
                clicked = self.safeClick('.btn-compare')
            if not clicked:
                self.driver.execute_script("document.location.href = '" + mainUrl + "';")
            self.waitUntilLoaded()


    def addToCart(self):
        for(i, item) in enumerate(self.items):
            if str(self.data['total_items']) != "1":
                self.loadUrl(self.items[item]['url'])
                self.waitUntilLoaded()
            if self.items[item]['qty'] > 1:
                self.checkCost = False
            for x in range(0, self.items[item]['qty']):
                self.addOfferToCart(False, '')
                self.loadUrl(self.items[item]['url'])
                self.waitUntilLoaded()

    def checkSpinner(self):
        time.sleep( 1 )
        ts = int(time.time())
        while int(time.time()) - ts < 60:
            try:
                loaded = self.elementExists('.spinner-backdrop') and self.elementExists('.spinner-backdrop .spinner') and self.driver.find_element_by_css_selector('.spinner-backdrop .spinner').is_displayed()
            except:
                loaded = True
            if not loaded:
                break
            time.sleep( 1 )

    def addOfferToCart(self, showCost = False, string = '', goOffers = True):

        #num offer
        offers = self.driver.find_elements_by_css_selector('.seller-card')
        numOffers = len(offers)
        if len(offers) < 1:
            print 'No Offers Found'
            self.completeProcess()

        offer = False
        for(i, offerObj) in enumerate(offers):
            if 'walmart' not in offerObj.find_element_by_css_selector('[data-tl-id="ProductSellerCardSellerInfo-SellerName"]').text.lower().strip():
                continue
            price = offerObj.find_element_by_css_selector('.price-group').get_attribute('aria-label').strip()
            price = price.replace(',', '.').replace(' ', '')
            newPrice = re.findall("\d+\.\d+", price.encode('utf-8'))

            if len(newPrice) < 1:
                print 'Impossible to get the item price'
                self.completeProcess()
            newPrice = float(newPrice[0])
            offer = offerObj
            break

        if offer == False:
            print 'No Offers Found'
            self.completeProcess()

        # if there is just one offer, Walmart doesn't allow to click "Add to cart"
        if numOffers == 1:
            # try to get the product url
            try:
                a = re.search('product\/(\d+)\/sellers', self.driver.current_url)
                sku = a.group(1)
            except:
                sku = ''

            if sku != '':
                print 'Going to the product page'
                self.loadUrl('https://www.walmart.com/ip/' + sku)
                self.waitUntilLoaded()
                time.sleep(1)
                # [data-tl-id="ProductPrimaryCTA-cta_add_to_cart_button"]
                clicked = self.safeClick('[data-tl-id="ProductPrimaryCTA-cta_add_to_cart_button"]', 0)
                if not clicked:
                    print string + 'Impossible click Add to cart button'
                    self.completeProcess()
                self.waitUntilLoaded()
                return True
        elif goOffers == True:
            self.goToOffersPage(self.driver.current_url)
            self.waitUntilLoaded()
            self.addOfferToCart(showCost, string, False)
            return True

        try:
            offer.find_element_by_css_selector('button').click()
            clicked = True
        except:
            print 'Try to press the button'
            clicked = self.safeClickObj(offer.find_element_by_css_selector('button'))
        time.sleep(randint(1, 5))

        print 'Adding the item to the basket'

        if not clicked:
            print string + 'Impossible click Add to cart button'
            self.completeProcess()
        self.waitUntilLoaded()

    def goToCartPage(self):
        clicked = self.safeClick('#header-Cart', 0)
        if not clicked:
            self.loadUrl('https://www.walmart.com/cart')

        self.waitUntilLoaded()
        time.sleep(3)

    def _return_exist_selector(self, selectros):
        for selector in selectros:
            if self.elementExists(selector):
                return selector

        return None

    def confirmBasket(self, checkCost = True):
        #time.sleep(60)
        if not self.elementExists('[data-automation-id="pickup-two-price-shipping-grand-total"]'):
            print 'Expected Cart Page'
            self.completeProcess()

        if checkCost:
            try:
                items = self.driver.find_elements_by_css_selector('[data-automation-id="pickup-two-price-shipping-grand-total-price"]')
                #total = self.driver.find_elements_by_css_selector('[data-automation-id="pickup-two-price-shipping-grand-total"]')[0].text.strip().replace(',', '').replace(' ', '')
                #ctotal = self.driver.find_elements_by_css_selector('[data-automation-id="pickup-two-price-shipping-grand-total"]')
                #newPrice = re.findall("\d+\.\d+", total.encode('utf-8'))
            except:
                print('Impossible to get the item price by css selector.')
                self.completeProcess()

            total = items[1].text.strip().replace(',', '').replace(' ', '')
            newPrice = re.findall("\d+\.\d+", total.encode('utf-8'))

            if len(newPrice) <1:
                print('Impossible to get the item price')
                print('items:')
                for item in items:
                    print(item.text)
                self.completeProcess()

            newPrice = float(newPrice[0])
            self.costPrice = newPrice
            print 'COST:' + total.encode('utf-8')

            # try to get the number of items in the cart
        numItems = 0
        stopAO = False
        try:
            objects = self.driver.find_elements_by_css_selector('[data-automation-id="cart-pos-pos-item-quantity"]')
            for (i, item) in enumerate(objects):
                try:
                    html = item.get_attribute('innerHTML').strip()
                    numItems = re.findall("\d+", html)[0]
                    if str(numItems) != str(self.data['total_quantity']):
                        stopAO = True
                    break
                except:
                    pass
        except:
            pass

        if stopAO:
            print 'The number of items in the cart does not match the number of item(s) in the order(s)'
            self.completeProcess()

        selectros = [
            '.hide-content-max-l [data-automation-id="cart-pos-proceed-to-checkout"]',
            '.hide-content-max-m [data-automation-id="cart-pos-proceed-to-checkout"]',
        ]


        checkout_button = self._return_exist_selector(selectros)
 
        if not checkout_button:
            print('Impossible find the proceed to checkout button')
            self.completeProcess(dont_close_broser=True)

        click = self.safeClick(checkout_button, attempt=12)
        if not click:
            print('Impossible click the proceed to checkout button')
            self.completeProcess()

        self.waitUntilLoaded()


    def confirmShipping(self):
        if self.elementExists('[data-automation-id="fulfillment"] .CXO_module_header_edit_button'):
            self.safeClick('[data-automation-id="fulfillment"] .CXO_module_header_edit_button')
            self.waitUntilLoaded()

        if self.isLoginPage():
            self.doLogin()


        if self.elementExists('.CXO-ShippingGroup'):
            print 'Confirm shipping'
            self.safeClick('[data-automation-id="fulfillment-continue"]')
            self.waitUntilLoaded()
            time.sleep(2)

        if self.elementExists('.CXO-ShippingGroup'):
            self.safeClick('[data-automation-id="fulfillment-continue"]')
            self.waitUntilLoaded()
            time.sleep(2)

    def checkAddressSpinner(self):
        time.sleep(1)
        ts = int(time.time())
        while int(time.time()) - ts < 60:
            loaded = self.elementExists('.spinner')
            if not loaded:
                break

            try:
                loaded = True
                imgs = self.driver.find_elements_by_css_selector('.spinner')
                for (i, item) in enumerate(imgs):
                    loaded = loaded and not item.is_displayed()
                if loaded:
                    break
            except:
                pass
            time.sleep(1)

        return

    def _submit_credit_card(self):
        click = self.safeClick('[data-automation-id="submit-payment-cc"]')

        if not click:
            print 'Error while confirming the payment method'
            self.completeProcess()

    def confirmCreditCard(self, submit_cc=False):
        self.checkSpinner()
        if not self.elementExists('.credit-card-cvv'):
            print 'Expected payment page'
            self.completeProcess()

        if self.elementExists('[data-name="GIFTCARD"]'):
            self.safeClick('[data-name="GIFTCARD"]')
            self.waitUntilLoaded()
            self.checkAddressSpinner()
            self.waitUntilLoaded()
            if self.elementExists('.gift-card-wrapper-inner .gift-card-checkbox'):
                print 'Choose Gift card'
                gc = self.driver.find_elements_by_css_selector('.gift-card-wrapper-inner .gift-card-checkbox')[0]
                if gc.get_attribute('checked') == None:
                    gc.click()
                    self.waitUntilLoaded()
                    self.checkAddressSpinner()
                    self.waitUntilLoaded()
            else:
                print 'There are no gift cards.'

            self.safeClick('[data-name="CREDITCARD"]')
            self.waitUntilLoaded()
            self.checkAddressSpinner()
            self.waitUntilLoaded()


        if self.elementExists('.credit-card-cvv'):
            self.write('.credit-card-cvv', self.user['cvv'])

        if submit_cc:
            self._submit_credit_card()

        self.waitUntilLoaded()
        self.checkAddressSpinner()
        self.waitUntilLoaded()
        if self.elementExists('.credit-card-cvv') and self.driver.find_element_by_css_selector('.credit-card-cvv').is_displayed():
            print 'Error while confirming the payment method'
            self.completeProcess()

        print 'Payment method confirmed'



    def placeOrder(self):
        click = self.safeClick('[data-automation-id="summary-place-holder"]')
        if not click:
            click = self.safeClick('[data-automation-id="summary-place-holder-alt"]')
            if not click:
                print 'Error while confirming the order'
                self.completeProcess()
        self.waitUntilLoaded()
        self.checkAddressSpinner()
        self.waitUntilLoaded()


    def deleteAddress(self):
        if str(self.delete_address) != '1':
            return True

        self.loadUrl('https://www.walmart.com/account/shippingaddresses')
        self.waitUntilLoaded()
        if self.isLoginPage():
            print "Login"
            self.doLogin()
            self.loadUrl('https://www.walmart.com/account/shippingaddresses')

        address = self.address
        addressName = address[0].replace("'", "\\'")
        found = False
        divs = self.driver.find_elements_by_css_selector('.recipient-name')
        for (i, elem) in enumerate(divs):
            if addressName in elem.text:
                found = elem
                break

        if found:
            item = found.find_element_by_xpath('.//ancestor::div[contains(concat(" ", @class, " "), " address-tile ")]')
            btn = item.find_element_by_css_selector('.delete-link')
            self.safeClickObj(btn)
            time.sleep(2)
            btn = item.find_element_by_css_selector('.delete-button')
            self.safeClickObj(btn)
            time.sleep(1)
            # sometimes Walmart asks for the password
            if self.elementExists('form.option-form-control') and self.elementExists('form.option-form-control [type="password"]'):
                self.write('form.option-form-control [type="password"]', self.user['password'], True)
                self.safeClick('form.option-form-control [type="submit"]')
                self.waitUntilLoaded()
                self.checkAddressSpinner()

            if self.elementExists('form.option-form-control') and self.elementExists('form.option-form-control [type="password"]'):
                self.write('form.option-form-control [type="password"]', self.user['password'], True)
                self.safeClick('form.option-form-control [type="submit"]')
                self.waitUntilLoaded()
                self.checkAddressSpinner()

            print 'Address deleted.'
