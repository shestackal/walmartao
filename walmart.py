# -*- coding: utf-8 -*-

import logging
import sys
import re

from lib import ApiClient, Walmart

local_storage = 'tmp/data/'
cookies_storage = 'tmp/cookies/'

def init():
    log_level = 'info'
    log_filename = 'test.log'
    log_format = '%(asctime)s - %(levelname)s: %(message)s'
    log_dateformat = '%d/%m/%Y %H:%M:%S'

    numeric_level = getattr(logging, log_level.upper(), None)

    logging.basicConfig(
        filename=log_filename,
        level=numeric_level,
        format=log_format,
        datefmt=log_dateformat
    )

    endpoint, order_id, param = read_argv()

    client = ApiClient(endpoint)
    data = client.get_json()

    '''
    proxy = 'socks5://127.0.0.1:9050'
    lib = Walmart(order_id, param,
                  local_storage=local_storage,
                  cookies_storage=cookies_storage,
                  proxy=proxy)

    '''

    lib = Walmart(order_id, param,
                  local_storage=local_storage,
                  cookies_storage=cookies_storage)

    lib.load_order_details(data)

    return lib


def read_argv():
    try:
        endpoint = sys.argv[1]
    except IndexError:
        endpoint = 'http://api.santogigliotti.com/autoOrdering/testwalmart'

    try:
        order_id = sys.argv[2]
    except IndexError:
        order_id = 'test'

    try:
        param = sys.argv[3]
    except IndexError:
        param = False

    return endpoint, order_id, param


def main(lib):

    #lib.loadOrderDetails()
    lib.startDriver(phantomjs=False)

    lib.loadUrl(lib.url)
    lib.waitUntilLoaded()

    #check if it's a valid product page
    if lib.elementExists('.error-ErrorPage') and lib.elementExists('.error-ErrorPage-copy'):
        print lib.driver.find_element_by_css_selector('.error-ErrorPage-copy').get_attribute('innerHTML').strip().lower()
        lib.completeProcess()

    if lib.get_cookie_file() != False:
        lib.loadCookies()
        lib.loadUrl(lib.url)

    lib.loadUrl(lib.url)
    lib.waitUntilLoaded()
    #check if it's a valid product page
    if lib.elementExists('.error-ErrorPage') and lib.elementExists('.error-ErrorPage-copy'):
        print lib.driver.find_element_by_css_selector('.error-ErrorPage-copy').get_attribute('innerHTML').strip().lower()
        lib.completeProcess()

    lib.loadUrl('https://www.walmart.com')
    lib.waitUntilLoaded()

    loginUrl = ''
    if lib.elementExists('.header-GlobalAccountFlyout-link.header-GlobalAccountFlyout-name'):
        loginUrl = lib.driver.find_element_by_css_selector('.header-GlobalAccountFlyout-link.header-GlobalAccountFlyout-name').get_attribute('href')
    elif lib.elementExists('.header-OffcanvasNav-entry--top'):
        loginUrl = lib.driver.find_element_by_css_selector('.header-OffcanvasNav-entry--top').get_attribute('href')
    elif lib.elementExists('.header-GlobalAccountFlyout-aLink'):
        loginUrl = lib.driver.find_element_by_css_selector('.header-GlobalAccountFlyout-aLink').get_attribute('href')
    if 'login' in loginUrl:
        lib.loadUrl(loginUrl)
        lib.waitUntilLoaded()
        if lib.isLoginPage():
            lib.doLogin()
    #add the address
    if int(lib.data['place_order']) == 0:
        lib.addAddress()
        print 'COMPLETE'
        lib.completeProcess()

    lib.goToCartPage()
    lib.emptyCart()
    lib.checkBasket()
    lib.loadUrl(lib.url)
    lib.waitUntilLoaded()
    lib.addToCart()
    lib.goToCartPage()

    lib.confirmBasket()
    if lib.isLoginPage():
        lib.doLogin()

    lib.checkAddressSpinner()
    lib.confirmShipping()
    lib.checkAddressSpinner()
    lib.addAddress2()
    lib.waitUntilLoaded()
    lib.checkAddressSpinner()


    #try to get the order taxes
    lib.taxes = 0
    if (lib.elementExists('.persistent-order-summary [data-automation-id="pos-tax-amount"]')):
        total = ''
        try:
            total = lib.driver.find_elements_by_css_selector('.persistent-order-summary [data-automation-id="pos-tax-amount"] span')[0].text.strip().replace(',', '').replace(' ', '')
        except:
            try:
                total = lib.driver.find_element_by_css_selector('.persistent-order-summary [data-automation-id="pos-tax-amount').text.strip().replace(',', '').replace(' ', '')
            except:
                pass
        if total != '':
            newPrice = re.findall("\d+\.\d+", total.encode('utf-8'))
            if len(newPrice) > 0:
                newPrice = float(newPrice[0])
                lib.taxes = newPrice
                print 'TAX:' + total.encode('utf-8')


    try:
        html_source = lib.driver.page_source
    except:
        html_source = ''

    try:
        text_file = open(local_storage + 'tax' + lib.order_id + '.html', "w")
        text_file.write(html_source.encode('utf-8').strip())
        text_file.close()
    except:
        pass

    #try to get the correct cost price
    if (lib.elementExists('.persistent-order-summary [data-automation-id="pos-grand-total-amount"]')):
        try:
            total = lib.driver.find_elements_by_css_selector('.persistent-order-summary [data-automation-id="pos-grand-total-amount"] span')[0].text.strip().replace(',', '').replace(' ', '')
        except:
            try:
                total = lib.driver.find_element_by_css_selector('.persistent-order-summary [data-automation-id="pos-grand-total-amount"]').text.strip().replace(',', '').replace(' ', '')
            except:
                print 'Impossible to get the cost price'
                lib.completeProcess()

        newPrice = re.findall("\d+\.\d+", total.encode('utf-8'))
        if len(newPrice) > 0:
            newPrice = float(newPrice[0])
            lib.costPrice = newPrice
            print 'COST:' + total.encode('utf-8')
            # check if I need to remove the taxes from the cost price
            if int(lib.data['exclude_tax']) > 0:
                lib.costPrice = lib.costPrice - lib.taxes



    #check the cost
    soldPrice = float(lib.data['price'])
    if lib.costPrice > soldPrice:
        print 'ERROR: The profit for this order is less than 0'
        lib.completeProcess()

    #
    input('ololo')

    lib.confirmCreditCard(submit_cc=False)

    lib.placeOrder()
    print 'Process completed correctly'
    lib.completeProcess()

    #This code is not needed at the moment

    lib.waitUntilLoaded()
    lib.checkAddressSpinner()
    lib.waitUntilLoaded()
    placed = False
    ts = int(time.time())
    while int(time.time()) - ts < 5:
        if lib.elementExists('.thank-you-order-id'):
            break
        time.sleep(1)

    if lib.elementExists('.thank-you-order-id'):
        orderId = lib.driver.find_element_by_css_selector('.thank-you-order-id').text.strip().replace("#", "")
        placed = True
        print 'ORDERID:' + str(orderId)
        print 'DISPATCH'
    elif lib.elementExists('.thankyou-order-id span'):
        orderId = lib.driver.find_element_by_css_selector('.thankyou-order-id span').text.strip().replace("Order #", "").replace(".", "").strip()
        placed = True
        print 'ORDERID:' + str(orderId)
        print 'DISPATCH'
    else:
        print 'There was a problem processing this order'
        lib.completeProcess()

    if placed:
        try:
            lib.deleteAddress()
        except:
            pass

    print "COMPLETE"
    lib.completeProcess()


if __name__ == '__main__':
    lib = init()
    #raise

    try:
        main(lib)
    except:
        logging.exception('Unknown error')
        #lib.completeProcess()

    print('exit')
