class WalmartError(Exception):
    pass


class ApiclientError(Exception):
    pass


class ServerError(ApiclientError):
    pass


class RetryExceededError(ApiclientError):
    pass


class WebdriverError(WalmartError):
    pass


class PageLoadError(WalmartError):
    pass


class LoginError(WalmartError):
    pass


class CaptchaExists(WalmartError):
    pass


class ElementNotFound(WalmartError):
    pass


class ElementValueError(WalmartError):
    pass


class ElementClickError(WalmartError):
    pass


class StoreError(WalmartError):
    pass


class SettingValueError(WalmartError):
    pass


class ConfirmationError(WalmartError):
    pass


class AlertRecieved(WalmartError):
    pass


class PageError(WalmartError):
    pass
